#! /usr/bin/python

from os import system, listdir
from sys import stdout

lst = [f for f in listdir('.') if f.endswith(".jpg")]
lst.sort()
lst = zip(lst[:-1], lst[1:])

run_cnt = 0
max_run_cnt = 4

print
for (i, (f1, f2)) in enumerate(lst):
    print '\t%d/%d\r' % (i, len(lst)),
    stdout.flush()

    if run_cnt == max_run_cnt - 1:
        system('convert %s %s -morph 10 dir/%05d_%%02d.jpg' % (f1, f2, i))
        run_cnt = 0
    else:
        run_cnt += 1
        system('convert %s %s -morph 10 dir/%05d_%%02d.jpg &' % (f1, f2, i))
