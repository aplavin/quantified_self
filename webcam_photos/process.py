#!/usr/bin/python

import sqlite3
import os
import os.path, time

dbconn = sqlite3.connect('images.db')
dbconn.text_factory = str

dbconn.execute('CREATE TABLE IF NOT EXISTS Images(TimeStamp REAL, FileName TEXT)')

for fname in sorted(os.listdir('.')):
    if not fname.endswith('.jpg'):
        continue

    ctime = os.path.getctime(fname)
    print ctime
    dbconn.execute('INSERT INTO Images VALUES (?, ?)', (ctime, fname))

dbconn.commit()
dbconn.close()
