#!/usr/bin/python

import os
import re
import feedparser
import sqlite3
import time

conn = sqlite3.connect('database.db')
conn.text_factory = str
conn.execute('CREATE TABLE IF NOT EXISTS Queries(\
    Id TEXT PRIMARY KEY ON CONFLICT IGNORE, Query TEXT, Url TEXT, Category TEXT, TimeStamp INT)')

xmlfiles = [f for f in os.listdir('.') if re.match(r'hist_\d+-\d+-\d+\.xml', f)]
rows = []
for i, xmlfile in enumerate(xmlfiles):
    if i % 10 == 0: print i, len(xmlfiles)

    feed = feedparser.parse(xmlfile)
    for en in feed.entries:
        row = (
            en['id'],
            en['title'],
            en['link'],
            en['tags'][0]['term'],
            time.mktime(en['published_parsed']))
        rows.append(row)

print len(rows), len(set(rows))
conn.executemany('INSERT INTO Queries VALUES (?,?,?,?,?)', rows)
conn.commit()
conn.close()
