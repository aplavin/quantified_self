
import sqlite3
import re
from itertools import islice


def log_to_tuples(log, beg):
    for line in log:
        if line.startswith(beg):
            line = re.sub('^.*?\\(', '', line[:-2])
            t = line.split(', ')
            yield (t[3], t[0], t[1], t[2])


with sqlite3.connect('database.db') as conn:
    conn.text_factory = str
    cur = conn.cursor()

    cur.execute('DELETE FROM KeyEvents')
    cur.execute('DELETE FROM MouseBtnEvents')
    cur.execute('DELETE FROM MouseMoves')

    with open('log.log', 'r') as logfile:
        cur.executemany('INSERT INTO KeyEvents VALUES (?,?,?,?)', log_to_tuples(logfile, 'KeyEvent'))
    with open('log.log', 'r') as logfile:
        cur.executemany('INSERT INTO MouseBtnEvents VALUES (?,?,?,?)', log_to_tuples(logfile, 'ButtonEvent'))
    with open('log.log', 'r') as logfile:
        cur.executemany('INSERT INTO MouseMoves VALUES (?,?,?,?)', log_to_tuples(logfile, 'MouseMove'))
