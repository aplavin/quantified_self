#! /usr/bin/python

from Xlib import X, XK, display
from Xlib.ext import record
from Xlib.protocol import rq
from time import time
import sys
import os
import sqlite3
from tendo import singleton
from random import randint


class Event:
    pass


class KeyEvent(Event):
    def __init__(self, key, type, win_class):
        self.key = key
        self.type = type
        self.win_class = win_class
        self.time = time()

    def to_tuple(self):
        return (self.time, self.key, self.type, self.win_class)

    def __str__(self):
        return 'KeyEvent(%s, %d, %s, %f)' % (self.key, self.type, self.win_class, self.time)


class ButtonEvent(Event):
    def __init__(self, key, type, win_class):
        self.key = key
        self.type = type
        self.win_class = win_class
        self.time = time()

    def to_tuple(self):
        return (self.time, self.key, self.type, self.win_class)

    def __str__(self):
        return 'ButtonEvent(%s, %d, %s, %f)' % (self.key, self.type, self.win_class, self.time)


class MouseMove(Event):
    def __init__(self, x, y, win_class):
        self.coords = (x, y)
        self.win_class = win_class
        self.time = time()

    def to_tuple(self):
        return (self.time, self.coords[0], self.coords[1], self.win_class)

    def __str__(self):
        return 'MouseMove(%d, %d, %s, %f)' % (self.coords[0], self.coords[1], self.win_class, self.time)


def keyboard_key(detail):
    keysym = local_dpy.keycode_to_keysym(detail, 0)

    for name in dir(XK):
        if name[:3] == "XK_" and getattr(XK, name) == keysym:
            return name[3:]

    mapping = {
        269025043: 'XF86AudioRaiseVolume',
        269025041: 'XF86AudioLowerVolume',
        269025026: 'XF86MonBrightnessUp',
        269025027: 'XF86MonBrightnessDown',
        269025044: 'XF86AudioPlay',
        269025045: 'XF86AudioStop',
        269025046: 'XF86AudioNext',
        269025047: 'XF86AudioPrev'
    }
    if keysym in mapping:
        return mapping[keysym]

    return "[%d]" % keysym


def mouse_button(detail):
    if detail == 1:
        return "MouseLeft"
    elif detail == 3:
        return "MouseRight"
    elif detail == 2:
        return "MouseMiddle"
    elif detail == 5:
        return "MouseWheelDown"
    elif detail == 4:
        return "MouseWheelUp"
    elif detail == 8:
        return "MouseBack"
    elif detail == 9:
        return "MouseForward"
    else:
        return "Mouse%d" % detail


def record_callback(reply):
    if reply.category != record.FromServer:
        return
    if reply.client_swapped:
        print "* received swapped protocol data, cowardly ignored"
        return
    if not len(reply.data) or ord(reply.data[0]) < 2:
        # not an event
        return

    data = reply.data
    while len(data):
        event, data = rq.EventField(None).parse_binary_value(data, record_dpy.display, None, None)

        key = keyboard_key(event.detail)
        mbtn = mouse_button(event.detail)

        win_class = xwindowinfo()['class']

        if event.type == X.KeyPress:
            e = KeyEvent(key, 1, win_class)
        elif event.type == X.KeyRelease:
            e = KeyEvent(key, 0, win_class)
        elif event.type == X.ButtonPress:
            e = ButtonEvent(mbtn, 1, win_class)
        elif event.type == X.ButtonRelease:
            e = ButtonEvent(mbtn, 0, win_class)
        elif event.type == X.MotionNotify:
            e = MouseMove(event.root_x, event.root_y, win_class)

        event_callback(e)


def event_callback(event):
    global dbconn

    if isinstance(event, KeyEvent):
        dbname = 'KeyEvents'
    elif isinstance(event, ButtonEvent):
        dbname = 'MouseBtnEvents'
    elif isinstance(event, MouseMove):
        dbname = 'MouseMoves'
    dbconn.execute('INSERT INTO %s VALUES (?,?,?,?)' % dbname, event.to_tuple())

    if randint(0, 100) <= 0:
        dbconn.commit()


def xwindowinfo():
    try:
        windowvar = local_dpy.get_input_focus().focus
        wmname = windowvar.get_wm_name()
        wmclass = windowvar.get_wm_class()
        wmhandle = str(windowvar)[20:30]
    except:
        ## This is to keep things running smoothly. It almost never happens, but still...
        return {"name": None, "class": None, "handle": None}
    if (wmname == None) and (wmclass == None):
        try:
            windowvar = windowvar.query_tree().parent
            wmname = windowvar.get_wm_name()
            wmclass = windowvar.get_wm_class()
            wmhandle = str(windowvar)[20:30]
        except:
            ## This is to keep things running smoothly. It almost never happens, but still...
            return {"name": None, "class": None, "handle": None}
    if wmclass == None:
        return {"name": wmname, "class": wmclass, "handle": wmhandle}
    else:
        return {"name": wmname, "class": wmclass[0], "handle": wmhandle}

dbconn = sqlite3.connect('/home/alexander/quantified_self/keylogger/database.db')
dbconn.text_factory = str

me = singleton.SingleInstance()

os.environ['XAUTHORITY'] = '/home/alexander/.Xauthority'
local_dpy = display.Display(':0')
record_dpy = display.Display(':0')


# Check if the extension is present
if not record_dpy.has_extension("RECORD"):
    print "RECORD extension not found"
    sys.exit(1)

# Create a recording context; we only want key and mouse events
ctx = record_dpy.record_create_context(
        0,
        [record.AllClients],
        [{
                'core_requests': (0, 0),
                'core_replies': (0, 0),
                'ext_requests': (0, 0, 0, 0),
                'ext_replies': (0, 0, 0, 0),
                'delivered_events': (0, 0),
                'device_events': (2, 6),
                'errors': (0, 0),
                'client_started': False,
                'client_died': False,
        }])

# Enable the context; this only returns after a call to record_disable_context,
# while calling the callback function in the meantime
record_dpy.record_enable_context(ctx, record_callback)
# Finally free the context
record_dpy.record_free_context(ctx)
