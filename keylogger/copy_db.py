#!/usr/bin/python

import time
import sqlite3

dbconn = sqlite3.connect('database_copy.db')
dbconn.text_factory = str

print 'Creating and filling TimeStamps...'
dbconn.execute('CREATE TABLE IF NOT EXISTS TimeStamps (TimeStamp INTEGER)')

start = int(time.mktime((2012, 1, 1, 0, 0, 0, 0, 0, 0)))
end = int(time.mktime((2013, 1, 1, 0, 0, 0, 0, 0, 0)))
step = 60 * 10

for i in range(start, end + 1, step):
    dbconn.execute('INSERT INTO TimeStamps VALUES(?)', (i,))

dbconn.commit()

print 'Creating indices on TimeStamp...'
dbconn.execute('CREATE INDEX idxTimeStamps ON TimeStamps (TimeStamp);')
dbconn.execute('CREATE INDEX idxKeyTimeStamps ON KeyEvents (TimeStamp);')
dbconn.execute('CREATE INDEX idxBtnTimeStamps ON MouseBtnEvents (TimeStamp);')
dbconn.execute('CREATE INDEX idxMouseTimeStamps ON MouseMoves (TimeStamp);')
dbconn.commit()

print 'Setting Bin and creating index...'
dbconn.execute('ALTER TABLE MouseMoves ADD COLUMN Bin INTEGER')
dbconn.execute('UPDATE MouseMoves SET Bin=MoveX/10*1000+MoveY/10')
dbconn.execute('CREATE INDEX idxCoordBin ON MouseMoves (Bin);')
dbconn.commit()

print 'Creating other indices...'
dbconn.execute('CREATE INDEX idxMouseBtnNames on MouseBtnEvents (KeyName)')

print 'Analyzing and vacuuming...'
dbconn.execute('ANALYZE')
dbconn.execute('VACUUM')
dbconn.commit()
